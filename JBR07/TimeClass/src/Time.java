public class Time {
    int hour;
    int minute;
    int second;
    
    public Time() {
        super();
    }

    public Time(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    @Override
    public String toString() {
        return "Time [hour=" + hour + ", minute=" + minute + ", second=" + second + "]";
    }

    public String nextSecond() {
        return String.format("Tang len 1s = %s", second + 1);
    }

    public String previousSecond() {
        second = second + 1;
        return String.format("Giam xuong 1s = %s", second - 1);
    }
}
