public class App {
    public static void main(String[] args) throws Exception {
        Time time1 = new Time(07,07,10);
        Time time2 = new Time(06,06,11);

        System.out.println(time1.toString());
        System.out.println(time2.toString());

        System.out.println(time1.nextSecond());
        System.out.println(time2.previousSecond());
    }
}
