public class App {
    public static void main(String[] args) throws Exception {
        Account account1 = new Account("10","Binh",0);
        Account account2 = new Account("20","Binh2",0,1000);

        System.out.println("Tai khoan 1");
        System.out.println(account1);
        System.out.println("Tai khoan 2");
        System.out.println(account2);

        System.out.println("Tai khoan 1 sau khi tang");
        System.out.println(account1.credit(2000));
        System.out.println("Tai khoan 2 sau khi tang");
        System.out.println(account2.credit(3000));

        System.out.println("Tai khoan 1 sau khi giam");
        account1.debit(1000);
        System.out.println("Tai khoan 2 sau khi giam");
        account2.debit(5000);

        System.out.println("Tai khoan 1 chuyen khoan di");
        account1.tranferTo(2000);
        System.out.println("Tai khoan 2 chuyen khoan di");
        account2.tranferTo(2000);
    }
}
