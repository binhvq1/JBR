public class Account {
    String id;
    String name;
    int amount;
    int balance = 0;

    public Account() {
        super();
    }

    public Account(String id,String name,int amount) {
        this.id = id;
        this.name = name;
        this.amount = amount;
    }

    public Account(String id,String name,int amount,int balance) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.balance = balance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id; 
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name; 
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount; 
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance; 
    }

    public int credit(int amount) {
        balance = balance + amount;
        return balance;
    }

    public void debit(int amount) {
        balance = balance - amount;
        if(amount <= balance) {
            System.out.println(balance);
        } else {
            System.out.println("Amount exceeded balance");
        }
    }

    public void tranferTo(int amount) {
        if(amount <= balance) {
            System.out.println("Tranfer amount to the given balance");
        } else {
            System.out.println("Amount exceeded balance");
        }
    }

    public String toString() {
        return id + " " + name + " " + amount + " " + balance;
    }
}
