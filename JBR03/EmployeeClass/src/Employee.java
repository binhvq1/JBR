public class Employee {
    int id;
    String firstname;
    String lastname;
    int salary;
    int annuasalary;
    double raisesalary;

    public Employee() {
        super();
    }

    public Employee(int id,String firstname,String lastname,int salary) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void getFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getAnnuaSalary(int id,String firstname,String lastname,int salary) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        return id + firstname + lastname + salary * 12;
    }

    public String raiseSalary(int id,String firstname,String lastname,int salary, double raisesalary) {
        return id + " " + firstname + " " + lastname + " " + raisesalary * salary;
    }

    public String toString() {
        return id + " " + firstname + " " + lastname + " " + salary * 12 ;
    }
}
