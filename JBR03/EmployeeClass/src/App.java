public class App {
    public static void main(String[] args) throws Exception {
        Employee employee1 = new Employee(10, "Binh", "Vo", 10000);
        Employee employee2 = new Employee(10, "Binh2", "Vo2", 20000);

        System.out.println("Luong Nhan Vien 1 x 12 tháng");
        System.out.println(employee1);
        System.out.println("Luong Nhan Vien 2 x 12 tháng");
        System.out.println(employee2);

        System.out.println("Luong nhan vien 1 sau khi tang");
        System.out.println(employee1.raiseSalary(10, "Binh", "Vo", 10000,1.1));

        System.out.println("Luong nhan vien 2 sau khi tang");
        System.out.println(employee1.raiseSalary(10, "Binh2", "Vo2", 20000,1.5));
    }
}
