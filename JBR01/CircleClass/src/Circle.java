public class Circle {
    double  radius = 1.0; //bán kính
    double  getArea = 1;
    double  getCircumference = 1;

    public Circle() {
        super();
    }

    public Circle(double  radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double  radius) {
        this.radius = radius;
    }
}
