public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.0);

        System.out.println("Dien tich circle1");
        System.out.println(Math.PI * Math.pow(circle1.radius, 2));
        System.out.println("Chu Vi circle1");
        System.out.println(Math.PI * 2 * circle1.radius);

        System.out.println("Dien tich circle2");
        System.out.println(Math.PI * Math.pow(circle2.radius, 2));
        System.out.println("Chu Vi circle2");
        System.out.println(Math.PI * 2 * circle2.radius);
    }
}
