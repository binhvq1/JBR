public class Rectangle {
    float length = 1.0f;
    float width = 1.0f;
    float getArea = 1.0f;
    float getPerimeter = 1.0f;

    public Rectangle() {
        super();
    }

    public Rectangle(float length , float width) {
        this.length = length;
        this.width = width;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getArea(float length,float width) {
        return length * width;
    }

    public float getPerimeter(float length,float width) {
        return (length + width) * 2;
    }
}
