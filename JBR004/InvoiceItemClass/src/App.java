public class App {
    public static void main(String[] args) throws Exception {
        InvoiceItem invoiceItem1 = new InvoiceItem("a","Gao",10,15000);
        InvoiceItem invoiceItem2 = new InvoiceItem("b","Muoi",20,20000);

        System.out.println("Doi tuong 1");
        System.out.println(invoiceItem2);
        System.out.println("Doi tuong 2");
        System.out.println(invoiceItem2);

        System.out.println("Tong gia");
        System.out.println(invoiceItem1.getTotal(10, 10) + invoiceItem2.getTotal(20.0, 20));
    }
}
